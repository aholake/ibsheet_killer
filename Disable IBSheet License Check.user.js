// ==UserScript==
// @name         IBSheet License Killer
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Disable IBSheet License Check
// @author       Loc Vo
// @include      http://nsostg.lotte.vn/**
// @include      http://my.local:8081/**
// @grant        unsafeWindow
// @run-at       document-start
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    unsafeWindow.oldAlert = window.alert;
    unsafeWindow.alert = function(msg) {
        if(msg.indexOf("License has expired") >= 0) {
            return;
        }
        unsafeWindow.oldAlert(msg);
    };
})();